-- local autocmd = vim.api.nvim_create_autocmd

-- Auto resize panes when resizing nvim window
-- autocmd("VimResized", {
--   pattern = "*",
--   command = "tabdo wincmd =",
-- })
vim.api.nvim_create_autocmd({"BufRead", "BufNewFile"}, {
    pattern = "Jenkinsfile",
    command = "set filetype=groovy",
})
vim.api.nvim_create_autocmd({"BufRead", "BufNewFile"}, {
    pattern = "*.sshconfig",
    command = "set filetype=sshconfig",
})
vim.opt.relativenumber = true
